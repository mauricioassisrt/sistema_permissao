-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 23-Maio-2020 às 19:16
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `aclteste`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(10) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `departamentos`
--

INSERT INTO `departamentos` (`id`, `nome`, `descricao`) VALUES
(1, 'teste', 'departamento teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_09_26_235536_create_Post_laravel_table', 1),
(4, '2016_09_28_005242_create_roles_table', 1),
(5, '2016_09_28_005317_create_permissions_table', 1),
(8, '2018_07_16_114832_create_notices_table', 2),
(9, '2018_07_17_192227_teste', 2),
(10, '2018_07_24_114256_add_empresa_e_name', 2),
(11, '2018_07_24_114828_add_alteracoes', 3),
(12, '2018_07_24_115133_add_empresa_e_name', 4),
(13, '2018_07_24_115308_adde', 5),
(14, '2018_07_24_121457_create_empresas_table', 6),
(16, '2018_08_06_004519_formapagamento', 7),
(18, '2018_08_07_120539_create_locadors_table', 9),
(19, '2018_08_06_132450_create_periodos_table', 10),
(20, '2018_08_08_110700_create_equipamentos_table', 11),
(21, '2018_08_14_110155_create_locacaos_table', 11),
(22, '2018_08_14_120952_create_itens_locacaos_table', 11),
(25, '2018_08_24_234817_create_itens_table', 12);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@laravel.com', '$2y$10$2sYGiyJBO2630CUPit2EA.U8c.NJYcYJJwu022aE1JYI9fKwU/9cC', '2018-07-25 23:31:28'),
('admin@laravel.com', '$2y$10$2sYGiyJBO2630CUPit2EA.U8c.NJYcYJJwu022aE1JYI9fKwU/9cC', '2018-07-25 23:31:28'),
('admin@laravel.com', '$2y$10$2sYGiyJBO2630CUPit2EA.U8c.NJYcYJJwu022aE1JYI9fKwU/9cC', '2018-07-25 23:31:28'),
('admin@laravel.com', '$2y$10$2sYGiyJBO2630CUPit2EA.U8c.NJYcYJJwu022aE1JYI9fKwU/9cC', '2018-07-25 23:31:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(5, 'View_user', 'Visualizar usuário', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(6, 'Edit_user', 'Editar usuário', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(7, 'Delete_user', 'Deletar usuário', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(8, 'Insert_user', 'Adicionar usuário', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(9, 'View_role', 'Visualizar role', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(10, 'Insert_role', 'Adicionar role', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(11, 'Role_permission', 'Relação role permission', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(12, 'View_permission', 'Visualizar permission', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(13, 'Insert_permission', 'Adicionar permission', '2018-07-14 01:27:37', '2018-07-14 01:27:37'),
(18, 'Delete_role', 'Deletar Role', '2018-07-19 23:42:14', '2018-07-19 23:42:14'),
(20, 'Autentica_user', 'Autenticar usuario', '2018-07-20 23:27:01', '2018-07-20 23:27:01'),
(21, 'Edit_user_logado', 'Editar usuário logado no sistema', '2018-07-23 19:22:23', '2018-07-23 19:22:23'),
(22, 'Edit_empresa', 'Editar uma empresa', '2018-07-24 18:34:42', '2018-07-24 18:34:42'),
(23, 'View_empresa', 'Visualiza uma empresa', '2018-07-24 18:35:16', '2018-07-24 18:35:16'),
(24, 'Insert_empresa', 'Insere uma nova empresa', '2018-07-24 18:35:36', '2018-07-24 18:35:36'),
(25, 'Delete_empresa', 'Apaga uma empresa determinada', '2018-07-24 18:35:53', '2018-07-24 18:35:53'),
(27, 'Edit_empresa_logado', 'O empresario pode editar informações relativas a sua empresa no sistema', '2018-07-31 17:47:53', '2018-07-31 17:47:53'),
(28, 'menu_cadastro', 'Aparece o menu cadastro na lateral esquerda da tela', '2018-07-31 18:53:11', '2018-07-31 18:53:11'),
(29, 'View_formaPagamento', 'Vizualiza forma de pagamento', '2018-08-06 17:48:15', '2018-08-06 17:48:15'),
(30, 'Insert_formaPagamento', 'Insere uma nova forma de pagamento', '2018-08-06 17:48:38', '2018-08-06 17:48:38'),
(31, 'Edit_formaPagamento', 'Edita uma forma de pagamento', '2018-08-06 17:48:52', '2018-08-06 17:48:52'),
(32, 'Delete_formaPagamento', 'Apaga forma de pagamento', '2018-08-06 17:49:07', '2018-08-06 17:49:07'),
(33, 'View_periodo', 'Visualiza um período de aluguel', '2018-08-06 20:01:38', '2018-08-06 20:01:38'),
(34, 'Insert_periodo', 'Insere um novo Período', '2018-08-06 20:17:15', '2018-08-06 20:17:15'),
(35, 'Edit_periodo', 'Edita um novo Periodo', '2018-08-06 20:17:28', '2018-08-06 20:17:28'),
(36, 'Delete_periodo', 'Apaga um periodo', '2018-08-06 20:17:40', '2018-08-06 20:17:40'),
(37, 'View_locador', 'Vizualiza os Locadores de Equipamentos', '2018-08-07 23:09:38', '2018-08-07 23:09:38'),
(38, 'Insert_locador', 'Insere um novo Locador', '2018-08-07 23:13:54', '2018-08-07 23:13:54'),
(39, 'Delete_locador', 'Apaga um locador', '2018-08-07 23:14:14', '2018-08-07 23:14:14'),
(40, 'Edit_locador', 'Edita um Locador', '2018-08-07 23:15:26', '2018-08-07 23:15:26'),
(41, 'View_equipamento', 'Visualiza', '2018-08-08 23:02:46', '2018-08-08 23:02:46'),
(42, 'Insert_equipamento', 'Insere', '2018-08-08 23:02:59', '2018-08-08 23:02:59'),
(43, 'Edit_equipamento', 'Edita', '2018-08-08 23:03:12', '2018-08-08 23:03:12'),
(44, 'Delete_equipamento', 'Apaga', '2018-08-08 23:03:21', '2018-08-08 23:03:21'),
(45, 'menu_cliente_logado', 'Menu cliente logado', '2018-08-10 23:00:54', '2018-08-10 23:00:54'),
(46, 'menu_empresa', 'Vizualiza o menu lateral da empresa logada', '2018-08-13 18:55:20', '2018-08-13 18:55:20'),
(47, 'View_Iten', 'Visualiza os itens locados até então', '2018-08-27 19:34:34', '2018-08-27 19:34:34'),
(48, 'Insert_locacao', 'Insere uma nova locação', '2018-08-28 00:35:49', '2018-08-28 00:35:49'),
(49, 'View_relatorio', 'Visualiza Relatórios', '2018-09-06 14:00:21', '2018-09-06 14:00:21'),
(50, 'View_peca', 'View_peca', '2019-06-06 20:49:55', '2019-06-06 20:49:55'),
(51, 'Edit_peca', 'Edit_peca', '2019-06-06 20:50:26', '2019-06-06 20:50:26'),
(52, 'Delete_peca', 'Delete_peca', '2019-06-06 20:50:44', '2019-06-06 20:50:44'),
(53, 'Insert_peca', 'Insert_peca', '2019-06-06 20:51:10', '2019-06-06 20:51:10'),
(54, 'dashboard_empresa', 'permissão para exibir dashboarda para empresa', '2020-05-23 19:51:47', '2020-05-23 19:51:47'),
(55, 'View_departamento', 'Visualiza um departamento', '2020-05-23 20:05:54', '2020-05-23 20:05:54'),
(56, 'Edit_departamento', 'Edita um departamento', '2020-05-23 20:06:11', '2020-05-23 20:06:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(6, 6, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(7, 7, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(8, 8, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(9, 9, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(10, 10, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(11, 11, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(12, 12, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(13, 13, 1, '2018-07-14 01:27:38', '2018-07-14 01:27:38'),
(18, 5, 1, NULL, NULL),
(34, 18, 1, NULL, NULL),
(43, 20, 1, NULL, NULL),
(44, 21, 2, NULL, NULL),
(45, 22, 1, NULL, NULL),
(46, 23, 1, NULL, NULL),
(47, 24, 1, NULL, NULL),
(48, 25, 1, NULL, NULL),
(49, 21, 4, NULL, NULL),
(50, 27, 4, NULL, NULL),
(51, 28, 1, NULL, NULL),
(52, 29, 1, NULL, NULL),
(53, 30, 1, NULL, NULL),
(54, 31, 1, NULL, NULL),
(55, 32, 1, NULL, NULL),
(59, 35, 1, NULL, NULL),
(60, 36, 1, NULL, NULL),
(61, 37, 4, NULL, NULL),
(62, 38, 4, NULL, NULL),
(63, 39, 4, NULL, NULL),
(64, 40, 4, NULL, NULL),
(65, 41, 4, NULL, NULL),
(66, 42, 4, NULL, NULL),
(67, 43, 4, NULL, NULL),
(68, 44, 4, NULL, NULL),
(69, 41, 1, NULL, NULL),
(70, 42, 1, NULL, NULL),
(71, 45, 4, NULL, NULL),
(72, 46, 4, NULL, NULL),
(73, 33, 4, NULL, NULL),
(74, 34, 4, NULL, NULL),
(75, 35, 4, NULL, NULL),
(76, 36, 4, NULL, NULL),
(77, 47, 4, NULL, NULL),
(78, 48, 4, NULL, NULL),
(79, 49, 4, NULL, NULL),
(80, 50, 4, NULL, NULL),
(81, 51, 4, NULL, NULL),
(82, 52, 4, NULL, NULL),
(83, 53, 4, NULL, NULL),
(84, 54, 4, NULL, NULL),
(85, 55, 4, NULL, NULL),
(86, 56, 4, NULL, NULL),
(87, 28, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Administrador do sistema', '2018-07-14 01:27:32', '2018-07-14 01:27:32'),
(2, 'sem_permissao', 'Sem perissão', '2018-07-21 01:05:15', '2018-07-21 01:05:15'),
(4, 'Empresario', 'Empresario, tem acesso a informações relacionada a sua empresa e aos seus clientes', '2018-07-24 22:14:24', '2018-07-24 22:14:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(15, 8, 1, NULL, NULL),
(18, 12, 4, NULL, NULL),
(20, 10, 1, NULL, NULL),
(22, 13, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(8, 'Admin geral1', 'evento.tads@gmail.com', '$2y$10$AjuGOdripkRGcXR4kaYhguyzYrdAAU7xpcUNYThhg28TBedCH0kh.', '9URAjPhrTxwKL5CulUSAQzg0lBcJ6m9D6GMGMyY3', '2018-07-24 00:39:18', '2018-09-04 13:54:06'),
(10, 'Mauricio de Assis', 'mauricioassisrt@gmail.com', '$2y$10$HTXtN.DGU5jKpmt.wezoNOYYsP8FFELsN.3nrkMcLJnoW13yIRP9G', '9zPUzo7AP9FUlIL0qWOLZaUiGdwDZeu1JyKOMYZNuQ0ocLnRxgw8prRwdUUM', '2018-07-25 23:34:47', '2019-06-03 22:02:26'),
(12, 'Teste novo Usuario', 'mauricioassishacker@hotmail.com', '$2y$10$vtO4iyz24mU11hgJHsRhp.J2O1X31Ss.CST3e7fKIXmsDlo6bHcC.', 'M0H3dP1sRu7hVIkvVEbt1SoXHXSq76VivOCcHhDs7rwXhTDPsrlpK8pagu2V', '2018-08-11 00:25:57', '2020-05-23 19:52:17'),
(13, 'Sergio De Assis', 'sergiodeassis2005@hotmail.com', '$2y$10$a8PUOSUxBInjMYwp..UzJOcbfJhLSvclKIspbIlZTDH2wXqtW9n4G', 'E2mVfo5InBNeTVKfaWICYl7UZsJUn6U7uStXkWQGRpy31CFr2x8TI6zEOQV1', '2018-09-01 16:20:09', '2018-09-01 16:20:09');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Índices para tabela `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Índices para tabela `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de tabela `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de tabela `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT de tabela `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
