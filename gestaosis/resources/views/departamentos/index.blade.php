@extends('adminapp')

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
      <h3 class="box-title">Departamentos</h3>
 
    <script type="text/javascript">
      function deletardados(e) {
        if (!(window.confirm("Tem certeza que deseja excluir esse usuario?")))
        e.returnValue = false;
      }
    </script>
    <table class="table table-hover table-responsive table-condensed">
      <thead>
        <tr>
          <th>
            ID
          </th>
          <th>
            NOME
          </th>
          <th>
            DESCRICAO
          </th>
          <th>
            Editar
          </th>
          <th>
            Deletar
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach ($objetoDepartamentos as $objetoDepartamento)
          <tr>
            <td>
              {{$objetoDepartamento->id}}
            </td>
            <td>
              {{$objetoDepartamento->nome}}
            </td>
            <td>
              {{$objetoDepartamento->descricao}}
            </td>
            <td>
            @can('Edit_departamento')
              <a href="{{url('departamentos/editar/'.$objetoDepartamento->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
            @else
              No edit
            @endcan
            </td>
            <td>
              <a href="{{url('departamentos/deletar/'.$objetoDepartamento->id)}}" class="btn btn-danger" onclick="return deletardados(event)"><span class="glyphicon glyphicon-remove"></span> Deletar</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    @endsection
