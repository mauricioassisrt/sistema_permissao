
@extends('adminapp')

@section('content')
<style type="text/css">
    #mymap {

        width: 100%;
        height: 500px;
    }
</style>



<section class="content">

    <div class="box box-primary">

        <div class="box-header with-border">
            <span class="glyphicon glyphicon-scale">
                <h3 class="box-title ">Painel Principal </h3>
            </span>

        </div>

        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    @if($errors->any())
                    <div class="alert alert-warning">
                        <strong>ATENÇÃO!</strong> {{$errors->first()}}
                    </div>

                    @endif
                    <div class="row">
                        @if(Session::has('autentica_ok'))
                        <div class="col-md-12">

                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Autenticado!</h4>
                                {{Session::get('autentica_ok')}}
                            </div>

                        </div>
                        @endif
                        @can('View_user')
                        <div class="col-md-6">
                            <div class="panel panel-default">


                                <div class="panel-body">
                                    <span class="glyphicon glyphicon-user gi-4x pull-left"></span>

                                    <h2 class="text-center">Usuarios <span class="label label-info">
                                            {{$total_user}}</span></h2>
                                </div>
                                <div class="panel-footer text-center">
                                    <a href="{{url('/users')}}" class="link_chamada">Ver usuarios <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('View_permission')
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <span class="glyphicon glyphicon-check gi-4x pull-left"></span><h2 class="text-center">Permissões <span class="label label-info">{{$total_permissions}}</span></h2>
                                </div>
                                <div class="panel-footer text-center">
                                    <a href="{{url('acl/permissions')}}" class="link_chamada">Ver Permissões <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @can('View_role')
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <span class="glyphicon glyphicon-file gi-4x pull-left"></span><h2 class="text-center">Funções <span class="label label-info">{{$total_roles}}</span></h2>
                                </div>
                                <div class="panel-footer text-center">
                                    <a href="{{url('acl/roles')}}" class="link_chamada">Ver Funções <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @can('View_empresa')
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <span class="glyphicon glyphicon glyphicon-home"></span><h2 class="text-center">Empresas <span class="label label-info">{{$total_empresas=!0 }}</span></h2>
                                </div>
                                <div class="panel-footer text-center">
                                    <a href="{{url('empresas')}}" class="link_chamada">Ver Empreas<span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                        @endcan


                    </div>
                    
                    <div class="col-md-3">

                        <div class="info-box ">
                            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Locadores </span>
                                <span class="info-box-number">
                                    {{$totaLocadores=!0}}
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Equipamentos</span>
                                <span class="info-box-number">
                                    {{$totalEqui=!0}}
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-calendar"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Locações Mês</span>
                                <span class="info-box-number">
                                    {{$totalMes=!0}}
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>

                    <div class="col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue"><i class="fa  fa-money"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Lucro Mês</span>
                                <span class="info-box-number">
                                    R$ {{$valorTotal=!0}},00
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>

                    <div class="col-md-12">                  
                        
                        <div  class="col-md-12" >
                            <div id="mymap" class="form-group"></div>
                        </div>
                    </div>

                        </tbody>
                    </table>
                    <script type="text/javascript">

                              

                    </script>
                  




                </div>
            </div>
        </div>

    </div>
</section>
@endsection
